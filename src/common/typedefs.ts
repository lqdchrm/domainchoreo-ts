export type TStichprobe = Array<number>;
export type TAnzahl = number;
export type TSumme = number;
export type TMittelwert = number;

export class TStatistik {
    stichprobe: TStichprobe;
    anzahl: TAnzahl;
    summe: TSumme;
    mittelwert: TMittelwert;
    
    print() { console.log(JSON.stringify(this, null, 2)); }
}