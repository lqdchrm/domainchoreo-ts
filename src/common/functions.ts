import { TStichprobe, TAnzahl, TSumme, TMittelwert, TStatistik } from '../common/typedefs'

export function stichprobeFromArray(...numbers: Array<number>): TStichprobe {
    return numbers;
}

export function anzahl(stichprobe: TStichprobe): TAnzahl {
    return stichprobe.length;
}

export function summe(stichprobe: TStichprobe): TSumme {
    return stichprobe.reduce((prev, cur) => prev + cur, 0);
}

export function mittelwert(anzahl: TAnzahl, summe: TSumme): TMittelwert {
    return summe / anzahl;
}

export function statistik(stichprobe: TStichprobe, anzahl: TAnzahl, summe: TSumme, mittelwert: TMittelwert): TStatistik {
    let result = new TStatistik();
    result.stichprobe = stichprobe;
    result.anzahl = anzahl;
    result.summe = summe;
    result.mittelwert = mittelwert;
    return result;
}