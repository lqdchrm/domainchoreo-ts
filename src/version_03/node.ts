export type FunctionNode<T> = {
    id: number,
    name: string,
    args: any[],
    result: T,
    eval: () => T,
    toString: string,
    print: (depth: number) => string
};

let nodeId = 0;

export function createNode<T>(f: Function, ...args: any[]): FunctionNode<T> {
    let result = null;
    let id = nodeId++;
    return {
        get id() { return id; },
        get name() { return f.prototype.constructor.name; },
        get args() { return args; },
        get result() { return result; },
        eval: () => {
            if (!result) {
                var mappedArgs = args.map(a => {
                    return a.eval ? a.eval() : a;
                });
                result = f.apply(f, mappedArgs);
            }
            return result;
        },
        get toString() { return `[${id}]: ${f.prototype.constructor.name}(${args.map(a => typeof a.toString == 'function' ? a.toString() : a.toString).join(',')})`; },
        print(depth = 0): string {
            let strIndent = '  ';
            let indent = depth ? Array(depth+1).join(strIndent) : '';
            let splitter = args.filter(a => a.print).length > 0 ? ',\n' : ','
            return `[${id}] ${indent}${f.prototype.constructor.name}(${args.filter(a => a.print).length > 0 ? '\n' : ''}${
                args.map(a => (a.print ? a.print(depth ? depth + 1 : 1): a.toString ? a.toString() : a)).join(splitter)
            })`;
        }
    }    
}