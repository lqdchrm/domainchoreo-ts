/******************************************************************************
* @module version_03
*
* @description
* Version 03: implementation with lazy functionNode objects => Expression Tree
* --------------------------------
* functions are stored as FunctionNodes along with their arguments in a tree structure
* 
* FeatureSet:
* [Y] Type-safety
* [Y] minimal number of calculations
* [Y] lazy execution
* [Y] step-by-step execution (normal debugging)
* [Y] reflection / self-descriptive
* 
******************************************************************************/

/******************************************************************************
███████╗██╗   ██╗███╗   ██╗ ██████╗████████╗██╗ ██████╗ ███╗   ██╗    ███╗   ██╗ ██████╗ ██████╗ ███████╗███████╗
██╔════╝██║   ██║████╗  ██║██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║    ████╗  ██║██╔═══██╗██╔══██╗██╔════╝██╔════╝
█████╗  ██║   ██║██╔██╗ ██║██║        ██║   ██║██║   ██║██╔██╗ ██║    ██╔██╗ ██║██║   ██║██║  ██║█████╗  ███████╗
██╔══╝  ██║   ██║██║╚██╗██║██║        ██║   ██║██║   ██║██║╚██╗██║    ██║╚██╗██║██║   ██║██║  ██║██╔══╝  ╚════██║
██║     ╚██████╔╝██║ ╚████║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║    ██║ ╚████║╚██████╔╝██████╔╝███████╗███████║
╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝    ╚═╝  ╚═══╝ ╚═════╝ ╚═════╝ ╚══════╝╚══════╝
******************************************************************************/
//#region func nodes
import { TStichprobe, TAnzahl, TSumme, TMittelwert, TStatistik } from '../common/typedefs'
import { stichprobeFromArray, anzahl, summe, mittelwert, statistik } from '../common/functions'
import { FunctionNode, createNode} from './node'

function stichprobeFromArrayAsNode(...numbers: number[]): FunctionNode<TStichprobe> {
    return createNode<TStichprobe>(stichprobeFromArray, ...numbers);
}
function anzahlAsNode(stichprobe: FunctionNode<TStichprobe>): FunctionNode<TAnzahl> {
    return createNode<TAnzahl>(anzahl, stichprobe);
}

function summeAsNode(stichprobe: FunctionNode<TStichprobe>): FunctionNode<TSumme> {
    return createNode<TSumme>(summe, stichprobe);
}

function mittelwertAsNode(anzahl: FunctionNode<TAnzahl>, summe: FunctionNode<TSumme>): FunctionNode<TMittelwert> {
    return createNode<TMittelwert>(mittelwert, anzahl, summe);
}

function statistikAsNode(stichprobe: FunctionNode<TStichprobe>, anzahl: FunctionNode<TAnzahl>, summe: FunctionNode<TSumme>, mittelwert: FunctionNode<TMittelwert>): FunctionNode<TStatistik> {
    return createNode<TStatistik>(statistik, stichprobe, anzahl, summe, mittelwert);
}
//#endregion

/******************************************************************************
 █████╗ ██████╗ ██████╗ ██╗     ██╗ ██████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗    ██╗      ██████╗  ██████╗ ██╗ ██████╗
██╔══██╗██╔══██╗██╔══██╗██║     ██║██╔════╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║    ██║     ██╔═══██╗██╔════╝ ██║██╔════╝
███████║██████╔╝██████╔╝██║     ██║██║     ███████║   ██║   ██║██║   ██║██╔██╗ ██║    ██║     ██║   ██║██║  ███╗██║██║     
██╔══██║██╔═══╝ ██╔═══╝ ██║     ██║██║     ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║    ██║     ██║   ██║██║   ██║██║██║     
██║  ██║██║     ██║     ███████╗██║╚██████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║    ███████╗╚██████╔╝╚██████╔╝██║╚██████╗
╚═╝  ╚═╝╚═╝     ╚═╝     ╚══════╝╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝    ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝ ╚═════╝
******************************************************************************/
//#region app
import { data } from '../../data/stichprobe'

function buildStatistik(nodeStichprobe: FunctionNode<TStichprobe>): FunctionNode<TStatistik> {
    let nodeAnzahl = anzahlAsNode(nodeStichprobe);
    let nodeSumme = summeAsNode(nodeStichprobe);
    let nodeMittelwert = mittelwertAsNode(nodeAnzahl, nodeSumme);
    let nodeStatistik = statistikAsNode(nodeStichprobe, nodeAnzahl, nodeSumme, nodeMittelwert);
    return nodeStatistik;
}

export function main() {
    let nodeStichprobe = stichprobeFromArrayAsNode(...data);
    let nodeStatistik = buildStatistik(nodeStichprobe);

    // now nodeStatitik is a tree of function nodes, so we can print it's structure
    console.log(nodeStatistik.print(0));

    // ... evaluate it lazily with memoization
    let result = nodeStatistik.eval();

    // ...and print it
    result.print();
}
//#endregion
