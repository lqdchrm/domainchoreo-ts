/******************************************************************************
* @module version_02
*
* @description
* Version 02: implementation with function objects and memoization
* --------------------------------
* functions evaluate lazily and are passed to other functions
* 
* FeatureSet:
* [Y] Type-safety
* [Y] minimal number of calculations
* [Y] lazy execution
* [Y] step-by-step execution (normal debugging)
* [N] reflection / self-descriptive
* 
******************************************************************************/

/******************************************************************************
██╗      █████╗ ███████╗██╗   ██╗    ███████╗██╗   ██╗███╗   ██╗ ██████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
██║     ██╔══██╗╚══███╔╝╚██╗ ██╔╝    ██╔════╝██║   ██║████╗  ██║██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
██║     ███████║  ███╔╝  ╚████╔╝     █████╗  ██║   ██║██╔██╗ ██║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗
██║     ██╔══██║ ███╔╝    ╚██╔╝      ██╔══╝  ██║   ██║██║╚██╗██║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║
███████╗██║  ██║███████╗   ██║       ██║     ╚██████╔╝██║ ╚████║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
╚══════╝╚═╝  ╚═╝╚══════╝   ╚═╝       ╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
******************************************************************************/
//#region functions
import { TStichprobe, TAnzahl, TSumme, TMittelwert, TStatistik } from '../common/typedefs'
import { stichprobeFromArray, anzahl, summe, mittelwert, statistik } from '../common/functions'
import { makeLazy} from './lazy'

function lazyStichprobeFromArray(...numbers: Array<number>): () => TStichprobe {
    return makeLazy(stichprobeFromArray, numbers);
}

function lazyAnzahl(stichprobe: TStichprobe): () => TAnzahl {
    return makeLazy(anzahl, stichprobe);
}

function lazySumme(stichprobe: TStichprobe): () => TSumme {
    return makeLazy(summe, stichprobe);
}

function lazyMittelwert(anzahl: TAnzahl, summe: TSumme): () => TMittelwert {
    return makeLazy(mittelwert, anzahl, summe);
}

function lazyStatistik(stichprobe: () => TStichprobe, anzahl: () => TAnzahl, summe: () => TSumme, mittelwert: () => TMittelwert): () => TStatistik {
    return makeLazy(statistik, stichprobe(), anzahl(), summe(), mittelwert());
}
//#endregion

/******************************************************************************
 █████╗ ██████╗ ██████╗ ██╗     ██╗ ██████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗    ██╗      ██████╗  ██████╗ ██╗ ██████╗
██╔══██╗██╔══██╗██╔══██╗██║     ██║██╔════╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║    ██║     ██╔═══██╗██╔════╝ ██║██╔════╝
███████║██████╔╝██████╔╝██║     ██║██║     ███████║   ██║   ██║██║   ██║██╔██╗ ██║    ██║     ██║   ██║██║  ███╗██║██║     
██╔══██║██╔═══╝ ██╔═══╝ ██║     ██║██║     ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║    ██║     ██║   ██║██║   ██║██║██║     
██║  ██║██║     ██║     ███████╗██║╚██████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║    ███████╗╚██████╔╝╚██████╔╝██║╚██████╗
╚═╝  ╚═╝╚═╝     ╚═╝     ╚══════╝╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝    ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝ ╚═════╝
******************************************************************************/
//#region app
import { data } from '../../data/stichprobe'

function buildStatistik(lazyStichprobe: () => TStichprobe): TStatistik {
    let resultAnzahl = lazyAnzahl(lazyStichprobe());
    let resultSumme = lazySumme(lazyStichprobe());
    let resultMittelwert = lazyMittelwert(resultAnzahl(), resultSumme());
    let resultStatistik = lazyStatistik(lazyStichprobe, resultAnzahl, resultSumme, resultMittelwert);
    return resultStatistik();
}

export function main() {
    let stichprobe = lazyStichprobeFromArray(...data);
    let resultStatistik = buildStatistik(stichprobe);
    resultStatistik.print();
}
//#endregion
