export function makeLazy<T>(fun: Function, ...args: any[]): () => T {
    let result = null;
    return () => {
        if (!result)
            result = fun.apply(fun, args);
        return result;
    }
}