/******************************************************************************
* @module version_01
*
* @description
* Version 01: Naive implementation
* --------------------------------
* functions evaluate directly and results are passed to other functions by value
* 
* FeatureSet:
* [Y] Type-safety
* [Y] minimal number of calculations
* [N] lazy execution
* [Y] step-by-step execution (normal debugging)
* [N] reflection / self-descriptive
* 
******************************************************************************/

/******************************************************************************
 █████╗ ██████╗ ██████╗ ██╗     ██╗ ██████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗    ██╗      ██████╗  ██████╗ ██╗ ██████╗
██╔══██╗██╔══██╗██╔══██╗██║     ██║██╔════╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║    ██║     ██╔═══██╗██╔════╝ ██║██╔════╝
███████║██████╔╝██████╔╝██║     ██║██║     ███████║   ██║   ██║██║   ██║██╔██╗ ██║    ██║     ██║   ██║██║  ███╗██║██║     
██╔══██║██╔═══╝ ██╔═══╝ ██║     ██║██║     ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║    ██║     ██║   ██║██║   ██║██║██║     
██║  ██║██║     ██║     ███████╗██║╚██████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║    ███████╗╚██████╔╝╚██████╔╝██║╚██████╗
╚═╝  ╚═╝╚═╝     ╚═╝     ╚══════╝╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝    ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝ ╚═════╝
******************************************************************************/
//#region app
import { data } from '../../data/stichprobe'
import { TStichprobe, TStatistik } from '../common/typedefs'
import { stichprobeFromArray, anzahl, summe, mittelwert, statistik } from '../common/functions'

function buildStatistik(stichprobe: TStichprobe): TStatistik {
    let resultAnzahl = anzahl(stichprobe);
    let resultSumme = summe(stichprobe);
    let resultMittelwert = mittelwert(resultAnzahl, resultSumme);
    let resultStatistik = statistik(stichprobe, resultAnzahl, resultSumme, resultMittelwert);
    
    return resultStatistik;
}

export function main() {
    let stichprobe = stichprobeFromArray(...data);
    let resultStatistik = buildStatistik(stichprobe);
    resultStatistik.print();
}
//#endregion