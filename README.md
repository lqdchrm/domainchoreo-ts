# Alternative versions of the domain choreographer demo

## Requirements (as I understood them)
* type-safety
* minimal number of calculations
* lazy execution
* step-by-step execution/debugging
* reflection / self-descriptive (used for drawing graphs)

## Code conventions
* There are common definitions for:
  * types in `common/typedefs`
  * basic calculations in `common/functions`

## Switching between the versions
* Just change the import statement to the respective folder in `src/index.ts`

## Build instructions

* You need to have a recent node version installed

### install
> npm install

### build
> npm run build

### test build from dist folder
> npm run test

### dev mode, aka watched build
> npm run dev

### normal local run
> npm run start (or F5 in Visual Studio Code)